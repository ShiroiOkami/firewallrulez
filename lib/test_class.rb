#!/usr/bin/env ruby
require_relative 'ASA'

t = ASA.new "192.168.1.1", "ucll", "ucllwachtwoord"
t.connect
puts "\n"
#out = t.get_version
#
#puts "#### TESTING VERSION RESULT ####"
#print out
#puts "\n\n"
#
#puts "#### TESTING ICMP RESULT XML ####"
#out = t.query("icmp", "xml", true,\
#              {interface: "inside", sip: "192.168.1.100", \
#                           type: "8", code: "0", dip: "8.8.8.8"})
#print out
#puts "\n\n"
#
#puts "#### TESTING ICMP RESULT STRING ####"
#out = t.query("icmp", "string", true, \
#              {interface: "inside", sip: "192.168.1.100", \
#                           type: "8", code: "0", dip: "8.8.8.8"})
#print out
#puts "\n\n"
#
#puts "#### TESTING ICMP RESULT HASH ####"
#out = t.query("icmp", "hash", true, \
#              {interface: "inside", sip: "192.168.1.100", \
#                           type: "8", code: "0", dip: "8.8.8.8"})
#print out
#puts "\n\n"
#
#puts "#### TESTING TCP/UDP RESULT STRING ####"
#out = t.query("tcp-udp", "xml", true, \
#                                 {protocol: "tcp", interface: "inside", \
#                              sip: "192.168.1.100", sport: "1974", \
#                              dip: "8.8.8.8", dport: "53"})
#print out
#puts "\n\n"
#
#puts "#### TESTING TCP/UDP RESULT STRING ####"
#out = t.query("tcp-udp", "string", true, \
#                                 {protocol: "tcp", interface: "inside", \
#                              sip: "192.168.1.100", sport: "1974", \
#                              dip: "8.8.8.8", dport: "53"})
#print out
#puts "\n\n"
#
#
#puts "#### TESTING TCP/UDP RESULT HASH ####"
#out = t.query("tcp-udp", "hash", true, \
#                                 {protocol: "tcp", interface: "inside", \
#                              sip: "192.168.1.100", sport: "1974", \
#                              dip: "8.8.8.8", dport: "53"})
#print out
#puts "\n\n"
#
#p out.inspect

out = t.query("tcp-udp", "xml", true, \
              { protocol: "udp", interface: "inside", \
                sip: "192.168.1.80", sport: "1942",
                dip: "8.8.4.4", dport: "137" })

puts out

t.disconnect()
