#!/usr/bin/env ruby

require 'nokogiri'
# Voeren het packet-tracer commando uit en passen de XML aan
# Declareren van de variabelen
# USAGE: rule-validator interface protocol action src_addr src_port dst_addr dst_port filename
interface = ARGV[0]
protocol = ARGV[1]
f_action = ARGV[2]
src_addr = ARGV[3]
src_port = ARGV[4]
dst_addr = ARGV[5]
dst_port = ARGV[6]
filename = ARGV[7]
#puts "packet-tracer.exp #{interface} #{protocol} #{src_addr} #{src_port} #{dst_addr} #{dst_port} #{filename} && output-trim #{filename}"
system("packet-tracer.exp #{interface} #{protocol} #{src_addr} #{src_port} #{dst_addr} #{dst_port} #{filename} && output-trim #{filename}")
#We zoeken hier de juiste tag (action)
@doc = Nokogiri::XML(File.open("#{filename}.xml"))
#puts "Loaded file..."
#puts "#{@doc}"
@doc.xpath("//firewall//result//action").each do |node|
        #puts "f_action=#{f_action}"
	if f_action == 'permit' then
          case node.content
          when "drop"
            puts "Valid"
          else
            puts "It is not necessary to create this rule."
          end
	elsif f_action == 'deny'
	  case node.content
          when "allow"
            puts "Valid"
          else
            puts "It is not necessary to create this rule."
          end
	else
	  puts "USAGE: rule-validator <interface> <protocol> <action> <source_address> <source_port> <destination_address> <destination_port>"
	end
	
end
system("rm #{filename}.xml")
