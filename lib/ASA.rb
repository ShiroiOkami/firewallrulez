require 'logger'
require 'net/ssh'
require 'net/ssh/telnet'
require 'active_support/core_ext/hash'

class ASA
  #### PUBLIC METHODS ####

  #### CALL METHODS ####


  def initialize(host, user, pass)
    @user = user
    @host = host
    @pass = pass
    @t = nil
  end

  #This method opens a SSH connection to the ASA,
  #It should be called before any command.
  def connect
    @t = Net::SSH::Telnet.new("Host" => @host,
                              "Username" => @user,
                              "Password" => @pass)
    @t.puts("enable")
    @t.puts("")
    @t.waitfor(%r{EindwerkAsa#})
  rescue => e
    abort "Error in connect, #{e.message}."
  end

  #This method attempts to close the current session.
  #Sometimes the ASA closes the connection first resulting in an error,
  #therefor we catch the Exception and silence it.
  def disconnect
    @t.close unless @t.nil?
    @t = nil
  rescue
    #Ignore closed connection error.
  end

  #This method is used to extract all version information of the ASA
  def get_version
    _result = nil
    _cmd = "show version"
    _result = trim(exec_cmd(_cmd))
    _result
  rescue => e
    abort "Error in get_version, #{e.message}."
  end

  #This method should be called for extracting result
  #It should always have a value
  def query(type, format, detail_flag, rule)
    _result = nil
    validate_type(type)
    validate_format(format)
    validate_detail_flag(detail_flag)
    validate_rulebase(rule)
    case type
    when "tcp-udp"
      _result = get_packet_tracer(format,detail_flag, rule)
    when "icmp"
      _result = get_icmp(format, detail_flag, rule)
    else
      abort "Unexpected type was passed."
    end
    _result
  rescue => e
    abort "Error in query, #{e.message}."
  end


  #### PRIVATE METHODS ####

  private

  #### TOOLS ####

  #This method executes the command given on the ASA and returns the output
  def exec_cmd(cmd)
    connect if !@t
    @t.cmd("#{cmd}")
  rescue => e
    abort "Error in exec_cmd, #{e.message}."
  end

  #This method is used to give the output back
  #of the TCP/UDP packet-tracer in the requested format
  def get_packet_tracer(format, detail_flag, rule={})
    _result = nil
    validate_rule_packet_tracer(rule)
    _cmd = "packet-tracer input #{rule[:interface]} \
    #{rule[:protocol]} #{rule[:sip]} #{rule[:sport]} \
    #{rule[:dip]} #{rule[:dport]} detailed xml"
    _result = exec_cmd(_cmd)
    result_validator(_result)
    _result = format_output(format, detail_flag, _result)
    _result
  rescue => e
    abort "Error in get_packet_tracer, #{e.message}."
  end

  #This method is used to give the output back
  #of the ICMP packet-tracer in the requested format
  def get_icmp(format, detail_flag, rule={})
    _result = nil
    validate_rule_icmp(rule)
    _cmd = "packet-tracer input #{rule[:interface]} icmp #{rule[:sip]} \
    #{rule[:type]} #{rule[:code]} #{rule[:dip]} xml"
    _result = exec_cmd(_cmd)
    result_validator(_result)
    _result = format_output(format, detail_flag, _result)
    _result
  rescue => e
    abort "Error in get_icmp, #{e.message}"
  end

  #This method is used to extract all version information of the ASA
  #This method gets rid of the prompt by trimming the result
  def trim(input)
    _output = nil
    _output = input.lines[2..-2].join
  rescue => e
    abort "Error in trim, #{e.message}."
  end

  #This method adds the <Firewall> root tag
  def add_root_tag(xml)
    xml = xml.prepend("<Firewall>\r\n")
    xml << "</Firewall>"
    xml
  rescue => e
    abort "Error in add_root_tag, #{e.message}."
  end

  #This method is used it format the output
  def format_output(format, detail_flag, output)
    _result = ""
    _result = add_root_tag(trim(output))
    if !(format == "xml")
      _result = get_hash(detail_flag, xml_to_hash_converter(_result))
      if format == "string"
        _result = get_string(detail_flag, _result)
      end
    end
    _result
  rescue => e
    abort "Error in format_output, #{e.message}."
  end

  #This method converts the xml to a hash
  def xml_to_hash_converter(xml)
    _hash = Hash.from_xml(xml)
  rescue => e
    abort "Error in xml_to_hash_converter, #{e.message} XML: #{xml}"
  end

  #This method returns the result as a hash
  def get_hash(detail_flag, hash={})
    _result = nil
    if !hash
      abort "No hash provided."
    end
    if detail_flag
    _result = hash["Firewall"]
    else
    _result = hash["Firewall"]["result"]
    end
    _result
  rescue => e
    abort "Error in get_hash, #{e.message}."
  end

  #This method returns the output in string format
  def get_string(detail_flag, hash={})
    _result = ""
    if !hash
      abort "No hash supplied."
    end
    if detail_flag
      _result = get_phases(hash["Phase"])
    end
    _result << "Result\n"
    hash["result"].each do |k,v|
      _result << "#{k}: #{v}\n"
    end

    _result
  rescue => e
    abort "Error in get_string, #{e.message}."
  end

  #This method extracts the Phase information for the string convertion
  def get_phases(hash={})
    _result = ""
    if !hash
      abort "No hash provided."
    end
    hash.each do |phase|
      _result << "Phase #{phase["id"]}\n"
      phase.each do |k, v|
        _result << "#{k}: #{v}\n"
      end
    end
    _result
  rescue => e
    abort "Error in get_phases, #{e.message}."
  end

  #### VALIDATORS ####

  #This method validates the type of request.
  #The possible requests are:
  #  tcp-udp -> request the output of the packet-tracer tcp/udp command
  #  icmp -> request the output of the packet-tracer icmp command
  def validate_type(type)
    if (type.nil? || type.blank? || \
    !(type == "icmp" || type == "tcp-udp"))
      abort "Invalid type: #{type}."
    end
    true
  rescue => e
    abort "Error in validate_typ, #{e.message}."
  end

  #This method validates the requested format
  #The possible formats are:
  #  string -> returns a formated string
  #  hash -> return a hash-object
  #  xml -> returns a XML-object
  def validate_format(format)
    if (format.nil? || format.blank? || \
      !( format == "string" || format == "hash" || format == "xml"))
    abort "Invalid format request: #{format}."
    end
    true
  rescue => e
    abort "Error in validate_format, #{e.message}."
  end

  #This method validates the detailed flag
  #The possible values are:
  #  true -> detailed version
  #  false -> result version
  def validate_detail_flag(detail_flag)
    if !!detail_flag == detail_flag
      true
    else
      abort "Invalid detail flag"
    end
  rescue => e
    abort "Error in validate_detail_flag, #{e.message}."
  end

  #This method validates the rulebase existance and for the common parameters
  #These parameters are:
  #  interface -> the incoming interface of the packet
  #  sip -> the source address of the rule
  #  dip -> the destination address of the rule
  def validate_rulebase(rule={})
    if !rule
      abort "No rule given."
    else
      if ((!rule[:interface] || rule[:interface].empty?) ||
      (!rule[:sip] || rule[:sip].empty?) ||
        (!rule[:dip] || rule[:dip].empty?))
        abort "Error: Missing interface, source ip or destination ip."
      end
    end
    true
  rescue => e
    abort "Error in validate_rulebase, #{e.message}."
  end

  #This method validates the remaining parameters for tcp/udp
  #These parameters are:
  #  protocol -> the protocol type (TCP/UDP)
  #  sport -> the source port
  #  dport -> the destination port
  def validate_rule_packet_tracer(rule={})
    if !rule
      abort "No rule given."
    end
    if ((!rule[:protocol] || rule[:protocol].empty?) ||
      (!rule[:sport] || rule[:sport].empty?) ||
      (!rule[:dport] || rule[:dport].empty?))
      abort "Error: Missing protocol, source port or destination port."
    end
    true
  rescue => e
    abort "Error in validate_rule_packet_tracer, #{e.message}."
  end

  #This method validates the remaining parameters for icmp
  #These parameters are:
  #  type -> the icmp-type
  #  code -> the icmp-code belonging to the icmp-type
  def validate_rule_icmp(rule={})
    if !rule
      abort "No rule given."
    end
    if ((!rule[:type] || rule[:type].empty?) ||
      (!rule[:code] || rule[:code].empty?))
      abort "Error: Missing ICMP type or ICMP code."
    end
    true
  rescue => e
    abort "Error in validate_rule_icmp, #{e.message}."
  end

  #This method validates the output of the ASA
  #If the output is too small or contains an error, we notify the user
  def result_validator(xml)
    if xml.lines.length < 10
      abort "Something went wrong while reading the output of the ASA."
    end
    _match = /ERROR:/ =~ xml
    unless _match.nil?
      abort "Wrong arguments send to ASA."
    end
  rescue => e
    abort "Error in result_validator, #{e.message}."
  end

end
