#/usr/bin/env ruby
require 'open3'

libpath = "/srv/http/firewallrulez/lib/"
scripts = [ "test_conn", "test_conn_bak" ]

STDOUT.sync = true

scripts.each do |s|
    fails = 0
    com = "ruby #{libpath}#{s} permit inside tcp 192.168.1.100 8.8.8.8 53"
    puts "testing #{com}"
    for attempt in 0...25
        print "Attempt [#{attempt}]: "
        stdout, stderr, status = Open3.capture3("#{com}")
        puts "SUCCESS=#{status.success?}"
        unless status.success?
            fails += 1
        end
    end
    puts "SCRIPT=#{s}, FAILS=#{fails}"
end
