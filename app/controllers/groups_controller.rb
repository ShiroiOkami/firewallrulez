class GroupsController < ApplicationController
  before_action :set_group, only: [:show, :edit, :update, :destroy]

  # GET /groups
  # GET /groups.json
  def index
    if params[:name].blank?
      @groups = Group.all
    else
      @groups = Group.select("id", "name").where("name LIKE ?", "#{params[:name]}%").order(:name).limit(10)

      @groups.each do |g|
        logger.debug("G: #{g.name}")
      end
      respond_to do |format|
        format.json { render json: @groups, :only => [:id, :name] }
      end
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # GET /groups/new
  def new
    @group = Group.new
    @group_services = "[]"
  end

  # GET /groups/1/edit
  def edit
    @group_services = @group.services.map do |item|
      [ item.id, item.name ]
    end
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(group_params)

    respond_to do |format|
      if @group.save
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update

    # if size == 1 then only the empty value was passed.
    if params[:group][:service_ids].size == 1
      @group_services = @group.services.map do |item|
        [ item.id, item.name ]
      end
    else
      @group.services.clear
      params[:group][:service_ids] ||= []
      params[:group][:service_ids].each do |k,v|
        @group.services << Service.find(k) unless k.empty?
      end
      @group_services = @group.services.map do |item|
        [ item.id, item.name ]
      end
    end

    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_url, notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name, { :service_ids => []})
    end
end
