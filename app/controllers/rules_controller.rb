class RulesController < ApplicationController
  before_action :set_rule, only: [:show, :edit, :update, :destroy]
  before_action :set_actions, only: [ :new, :edit, :update, :create ]
  before_action :authenticate_user!

  # GET /rules
  # GET /rules.json
  def index
    @user = current_user
    if current_user.admin?
      @rules = Rule.all
    else
      @rules = current_user.rules
    end
  end

  # GET /rules/1
  # GET /rules/1.json
  def show
    @x = eval(@rule.result)
    @phases = @x["Phase"]
    @result = [ @x["result"] ]
  end

  # GET /rules/new
  def new
    @rule = Rule.new
    @rule_services = "[]"
    @rule_groups = "[]"
  end

  # GET /rules/1/edit
  def edit
    @rule_services = @rule.services.map do |item|
      [ item.id, item.name ]
    end
    @rule_groups = @rule.groups.map do |item|
      [ item.id, item.name ]
    end
  end

  # POST /rules
  # POST /rules.json
  def create
    @rule = current_user.rules.new(rule_params)

    respond_to do |format|
      if @rule.save
        format.html {
          redirect_to @rule, notice: 'Rule was successfully created.'
        }
        format.json { render :show, status: :created, location: @rule }
      else
        format.html { render :new }
        format.json { render json: @rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rules/1
  # PATCH/PUT /rules/1.json
  def update
    authorize @rule

    # if size == 1 then only the empty value was passed.
    if params[:rule][:service_ids].size == 1
      @rule_services = @rule.services.map do |item|
        [ item.id, item.name ]
      end
    else
      @rule.services.clear
      params[:rule][:service_ids] ||= []
      params[:rule][:service_ids].each do |k,v|
        @rule.services << Service.find(k) unless k.empty?
      end
      @rule_services = @rule.services.map do |item|
        [ item.id, item.name ]
      end
    end

    # if size == 1 then only the empty value was passed.
    if params[:rule][:group_ids].size == 1
      @rule_groups = @rule.groups.map do |item|
        [ item.id, item.name ]
      end
    else
      @rule.groups.clear
      params[:rule][:group_ids] ||= []
      params[:rule][:group_ids].each do |k,v|
        @rule.groups << Group.find(k) unless k.empty?
      end
      @rule_groups = @rule.groups.map do |item|
        [ item.id, item.name ]
      end
    end

    respond_to do |format|
      if @rule.update(rule_params)
        format.html { redirect_to @rule, notice: 'Rule was successfully updated.' }
        format.json { render :show, status: :ok, location: @rule }
      else
        format.html { render :edit }
        format.json { render json: @rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rules/1
  # DELETE /rules/1.json
  def destroy
    authorize @rule
    @rule.destroy
    respond_to do |format|
      format.html { redirect_to rules_url, notice: 'Rule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rule
      @rule = Rule.find(params[:id])
    end

    def set_actions
      @actions = Rule::ACTIONS
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rule_params
      params.require(:rule).permit(:description, :action, :src_addr, :sport, :dst_addr, {:service_ids => []}, {:group_ids => []})
    end
end
