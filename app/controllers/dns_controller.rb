class DnsController < ApplicationController

  def reverse
    if params[:ip].blank?
      render json: '[{ "error": "Cannot resolve a blank IP." }]'
    else
      begin
        logger.debug("IP: #{params[:ip]}")
        host = Resolv::getname("#{params[:ip]}")
        message = "[{ \"hostname\": \"%s\" }]" % [ host ]
        render json: "#{message}"
      rescue => e
        message = "[{ \"error\": \"%s\" }]" % [ e.message ]
        render json: "#{message}"
      end
    end
  end

  def forward
    if params[:name].blank?
      render json: '[{ "error": "cannot resolve blank name." }]'
    else
      begin
        ip = Resolv::getaddress("#{params[:name]}")
        message = "[{ \"ip\": \"%s\" }]" % [ ip ]
        render json: "#{message}"
      rescue => e
        message = "[{ \"error\": \"%s\" }]" % [ e.message ]
        render json: "#{message}"
      end
    end
  end

  private

  def dns_params
    params.require(:dns).permit(:ip, :name)
  end

end
