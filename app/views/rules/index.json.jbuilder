json.array!(@rules) do |rule|
  json.extract! rule, :id, :protocol, :src_t, :src_address, :src_mask, :dst_t, :dst_address, :dst_mask, :port_op, :port_from, :port_to, :action
  json.url rule_url(rule, format: :json)
end
