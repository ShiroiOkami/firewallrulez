json.array!(@services) do |service|
  json.extract! service, :id, :name, :protocol, :port
  json.url service_url(service, format: :json)
end
