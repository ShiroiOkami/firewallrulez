class Group < ActiveRecord::Base
  has_and_belongs_to_many :services

  validates :name, :services, presence: true
  validates :name, length: { maximum: 20 }
  validates :name, uniqueness: true
end
