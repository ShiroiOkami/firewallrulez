class Service < ActiveRecord::Base
  has_and_belongs_to_many :rules
  has_and_belongs_to_many :groups

  PROTOCOLS = %w(tcp udp)

  validates :name, :protocol, :port, presence: true
  validates :name, length: { maximum: 20 }
  validates :name, uniqueness: true
  validates :protocol, :inclusion => { :in => PROTOCOLS } 
  validates :port, inclusion: { in: 1..65535 }
  validates :port, uniqueness: { scope: :protocol, message: "service already defined." }
end
