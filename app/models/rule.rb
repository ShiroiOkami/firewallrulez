require 'ASA'
require 'open3'
require 'resolv'
require 'uri'

class Rule < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :services
  has_and_belongs_to_many :groups

  before_validation :set_sport

  ACTIONS = %w(permit deny)

  validates :action, :src_addr, :dst_addr, presence: true

  validates :description, length: { maximum: 255 }
  validates :action, :inclusion => { :in => ACTIONS }
  validates :sport, :numericality => { :greater_than => 0,
                                       :less_than => 65536,
                                       :allow_blank => true }

  validate :presence_of_services_or_groups
  validate :valid_src, :valid_dst
  validate :valid_rule

  private

  def set_sport
    self.sport = rand(1025..65535) if self.sport.blank?
  end

  def default_port(uri)
    # Returns the default port for a given URI if it is known, else nil.
    _port = URI.parse(URI.encode(uri)).port
  rescue
  end

  def domain(uri)
    # Returns the domain with a URI scheme
    uri.gsub(/^[a-zA-Z]*:\/\//, '')
  end

  def hostname_ip(hostname)
    # Returns the IP of a domain.
    _domain = domain(hostname)
    _encoded = URI.encode(_domain)
    Resolv.getaddress(_encoded)
  rescue Resolv::ResolvError
  end

  def random_ip(network)
    # Returns a random IP address from a network.
    _net = NetAddr::CIDR.create(network)
    unless _net.netmask == "/32"
      _net = NetAddr::CIDR.create("#{_net.network}#{_net.netmask}")
      _net = _net[rand 1...(_net.size-1)]
    end
    _net.ip
  rescue ValidationError # Was not a valid IP.
  end

  def ip_address(address)
    # Returns an IP address from either the hostname or a network.
    if hostname_ip(address).nil?
      return random_ip(address)
    end
    hostname_ip(address)
  rescue
  end

  def valid_src
    NetAddr::CIDR.create(ip_address(src_addr))
  rescue
    self.errors[:base] << "The source address #{src_addr} is not valid."
  end

  def valid_dst
    NetAddr::CIDR.create(ip_address(dst_addr))
  rescue
    self.errors[:base] << "The destination address #{dst_addr} is not valid."
  end

  def interface(ipaddr)
    # TODO: interfaces should be obtained from the ASA.
    best = 2**32
    candidate = nil
    interfaces = {}
    interfaces["inside"] = [ "192.168.1.0/24" ]
    interfaces["outside"] = [ "193.191.187.0/25" ]
    interfaces["humanresources"] = [ "10.10.10.0/24" ]
    interfaces.each do |interface, subnets|
      subnets.each do |subnet|
        net = NetAddr::CIDR.create(subnet)
        if net.contains?(ipaddr) && net.size < best
          best = net.size
          candidate = interface
        end
      end
    end
    candidate
  end

  # A rule should have at least a group or a service linked to it.
  def presence_of_services_or_groups
    if services.blank? and self.groups.blank?
      self.errors[:base] << "Please select at least 1 service or group."
    end
  end

  def valid_rule
    unless self.errors.any?
      _random_src = ip_address(src_addr)
      _random_dst = ip_address(dst_addr)
      _interface = interface(_random_src) || "outside"
      logger.debug("INTERFACE: #{_interface}")
      begin
        asa = ASA.new(ENV["FW_IP"], ENV["FW_USER"], ENV["FW_PASSWORD"])
        asa.connect
        # code
        unless services.blank?
          self.services.each do |service|
            response = asa.query("tcp-udp", "hash", true, {
              :interface => "#{_interface}",
              :protocol => "#{service.protocol}",
              :sip => "#{_random_src}",
              :sport => "#{self.sport}",
              :dip => "#{_random_dst}",
              :dport => "#{service.port}"
            })
            unless response.nil?
              self.result = "#{response.inspect}"
              case response["result"]["action"]
              when "allow"
                errors[:base] << "The service '#{service.name}'\
                (#{service.protocol}-#{service.port})\
                is already allowed" if "#{action}" == "permit"
              when "drop"
                errors[:base] << "The service '#{service.name}'\
                (#{service.protocol}-#{service.port})\
                is already dropped" if "#{action}" == "deny"
              else
                # pass response to controller
              end
            end
          end
        end
        # GROUPS
        unless groups.blank?
          self.groups.each do |group|
            group.services.each do |service|
              logger.debug("Testing GroupService: #{service.name} for #{group.name}")
              response = asa.query("tcp-udp", "hash", true, {
                :interface => "#{_interface}",
                :protocol => "#{service.protocol}",
                :sip => "#{_random_src}",
                :sport => "#{self.sport}",
                :dip => "#{_random_dst}",
                :dport => "#{service.port}"
              })
              unless response.nil?
                self.result = "#{response.inspect}"
                case response["result"]["action"]
                when "allow"
                  errors[:base] << "The service '#{service.name}'\
                  (#{service.protocol}-#{service.port})\
                  is already allowed" if "#{action}" == "permit"
                when "drop"
                  errors[:base] << "The service '#{service.name}'\
                  (#{service.protocol}-#{service.port})\
                  is already dropped" if "#{action}" == "deny"
                else
                  #pass response to controller
                end
              end
            end
          end
        end
      rescue => e
        errors[:base] << e.message
      ensure
        asa.disconnect
      end
    end
  end

  public

end
