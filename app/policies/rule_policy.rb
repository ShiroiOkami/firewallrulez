class RulePolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @rule = model
  end

  def index?
  end

  def show?
  end

  def update?
    @current_user.admin? or @rule.user_id == @current_user.id
  end

  def destroy?
    @current_user.admin? or @rule.user_id == @current_user.id
  end

end
