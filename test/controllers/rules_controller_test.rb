require 'test_helper'

class RulesControllerTest < ActionController::TestCase
  setup do
    @rule = rules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rule" do
    assert_difference('Rule.count') do
      post :create, rule: { action: @rule.action, dst_address: @rule.dst_address, dst_mask: @rule.dst_mask, dst_t: @rule.dst_t, port_from: @rule.port_from, port_op: @rule.port_op, port_to: @rule.port_to, protocol: @rule.protocol, src_address: @rule.src_address, src_mask: @rule.src_mask, src_t: @rule.src_t }
    end

    assert_redirected_to rule_path(assigns(:rule))
  end

  test "should show rule" do
    get :show, id: @rule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rule
    assert_response :success
  end

  test "should update rule" do
    patch :update, id: @rule, rule: { action: @rule.action, dst_address: @rule.dst_address, dst_mask: @rule.dst_mask, dst_t: @rule.dst_t, port_from: @rule.port_from, port_op: @rule.port_op, port_to: @rule.port_to, protocol: @rule.protocol, src_address: @rule.src_address, src_mask: @rule.src_mask, src_t: @rule.src_t }
    assert_redirected_to rule_path(assigns(:rule))
  end

  test "should destroy rule" do
    assert_difference('Rule.count', -1) do
      delete :destroy, id: @rule
    end

    assert_redirected_to rules_path
  end
end
