# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160614094013) do

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups_rules", id: false, force: :cascade do |t|
    t.integer "group_id", null: false
    t.integer "rule_id",  null: false
  end

  add_index "groups_rules", ["group_id", "rule_id"], name: "index_groups_rules_on_group_id_and_rule_id"

  create_table "groups_services", id: false, force: :cascade do |t|
    t.integer "group_id",   null: false
    t.integer "service_id", null: false
  end

  add_index "groups_services", ["group_id", "service_id"], name: "index_groups_services_on_group_id_and_service_id"

  create_table "rules", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "action"
    t.string   "description"
    t.string   "src_addr"
    t.string   "dst_addr"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "result"
    t.string   "sport"
  end

  add_index "rules", ["user_id"], name: "index_rules_on_user_id"

  create_table "rules_services", id: false, force: :cascade do |t|
    t.integer "service_id", null: false
    t.integer "rule_id",    null: false
  end

  add_index "rules_services", ["service_id", "rule_id"], name: "index_rules_services_on_service_id_and_rule_id"

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.string   "protocol"
    t.integer  "port"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
