# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Configure the initial admin
user = CreateAdminService.new.call
puts '[AdminService] initial admin user created: ' << user.email

#services = [
#  [ "HTTP", "tcp", 80 ],
#  [ "HTTPS", "tcp", 443 ],
#  [ "DNS", "udp", 53 ],
#  [ "HTTP_PROXY", "tcp", 8080 ],
#  [ "SSH", "tcp", 22 ],
#  [ "FTP_CONTROL", "tcp", 20 ],
#  [ "FTP_DATA", "udp", 21 ]
#]
#
#services.each do |name, proto, port|
#  Service.create(name: name, protocol: proto, port: port )
#end
#
#group_www = Group.new
#group_www.name = "www"
#group_www.services << Service.find(1)
#group_www.services << Service.find(2)
#group_www.save!
#
#group_ftp = Group.new
#group_ftp.name = "ftp"
#group_ftp.services << Service.find(6)
#group_ftp.services << Service.find(7)
#group_ftp.save!

services = {}

File.open("/etc/services", "r") do |infile|
  while (line = infile.gets)
    match = /(?<service>[a-z0-9-]*)[ |\t]*(?<port>[0-9]*)\/(?<proto>(udp|tcp))[ |\t]*.*/.match("#{line}")
    unless match.nil?
      service, port, proto = match.captures
      if services.key?("#{service}")
        services["#{service}"] << [ :port => "#{port}", :proto => "#{proto}" ]
      else
        services["#{service}"] = [ [ :port => "#{port}", :proto => "#{proto}" ] ]
      end
    end
  end
end

already_done = {}

services.each_key do |k|
  if services[k].count > 1
    puts "\nNew Group: #{k}"
    g = Group.new
    g.name = "#{k}"
  else
    puts "\nNew Service: #{k} "
  end
  services[k].each do |e|
    e.each do |serv|
      unless already_done.key?"#{serv[:port]}"
        already_done["#{serv[:port]}"] = []
      end
      already_done["#{serv[:port]}"] << "#{serv[:proto]}"
      if services[k].count > 1
        puts "Adding %s-%s to %s." % [ "#{k}", "#{serv[:proto]}", "#{k}" ]
        s = Service.create(name: "#{k}-#{serv[:proto]}",
                           protocol: "#{serv[:proto]}",
                           port: "#{serv[:port]}")
        g.services << s
      else
        Service.create(name: "#{k}", protocol: "#{serv[:proto]}",
                       port: "#{serv[:port]}")
      end
    end
  end
  if services[k].count > 1
    g.save!
  end
end

puts already_done

ActiveRecord::Base.transaction do
  (1...65535).each do |n|
    unless already_done.key?("#{n}")
      puts "Creating %s-tcp and %s-udp" % [ "#{n}", "#{n}" ]
      s = Service.create(name: "#{n}-udp",
                         protocol: "udp",
                         port: "#{n}")
      s = Service.create(name: "#{n}-tcp",
                         protocol: "tcp",
                         port: "#{n}")
    else
      already_done["#{n}"].each do |e|
        unless already_done["#{n}"].include?("udp")
          s = Service.create(name: "#{n}-udp",
                             protocol: "udp",
                             port: "#{n}")
          puts "Creating %s-udp" % [ "#{n}" ]
        end
        unless already_done["#{n}"].include?("tcp")
          s = Service.create(name: "#{n}-tcp",
                             protocol: "tcp",
                             port: "#{n}")
          puts "Creating %s-tcp" % [ "#{n}" ]
        end
      end
    end
  end
end
