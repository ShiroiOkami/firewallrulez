class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :protocol
      t.integer :port

      t.timestamps null: false
    end
  end
end
