class CreateJoinTableRuleServices < ActiveRecord::Migration
  def change
    create_join_table :services, :rules do |t|
      t.index [:service_id, :rule_id]
      # t.index [:rule_id, :service_id]
    end
  end
end
