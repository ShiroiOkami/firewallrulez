class CreateJoinTableRuleGroups < ActiveRecord::Migration
  def change
    create_join_table :groups, :rules do |t|
      t.index [:group_id, :rule_id]
      # t.index [:rule_id, :group_id]
    end
  end
end
