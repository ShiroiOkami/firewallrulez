class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.belongs_to :user, index: true
      t.string :action
      t.string :description
      t.string :src_addr
      t.string :dst_addr

      t.timestamps null: false
    end
  end
end
